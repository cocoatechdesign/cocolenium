package com.cocoatechdesign.cocolenium;

import com.cocoatechdesign.cocolenium.resources.MobileDevices;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static java.lang.System.out;

/**
 * Created by IntelliJ IDEA
 * User: Saray Gómez
 * Email: sgomez@userzoom.com
 * On 3/8/16
 * To cocolenium project
 */
public class PageMobile {
    DesiredCapabilities capabilities;
    public PageMobile(String device) throws MalformedURLException {

        switch (devices.valueOf(device)) {
            case iOS:
                new DesiredCapabilities();
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, MobileDevices.IPHONE6S_UDID.name);
                capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
                capabilities.setCapability(MobileCapabilityType.UDID, MobileDevices.IPHONE6S_UDID.udid);
                capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Firefox");
                capabilities.setCapability("bundleId", "userzoom.com.UZSurveyTool");
                capabilities.setCapability(IOSMobileCapabilityType.LAUNCH_TIMEOUT, 500000);
                capabilities.setCapability("processArguments", "\" -e QA MOLA \"");
                capabilities.setCapability("sendKeyStrategy", "setValue");

                IOSDriver<MobileElement> iDriver = new IOSDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
                iDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

                System.out.println("iOS Native SetUp is successful and Appium Driver is launched successfully");
                break;
            case Android:
                new DesiredCapabilities();
                capabilities = new DesiredCapabilities();
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android");
                capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
                capabilities.setCapability("device", "android");
                capabilities.setCapability("app", "com.userzoom.uzapp");
                AndroidDriver<MobileElement> aDriver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
                aDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
                out.println("Android Native SetUp is successful and Appium Driver is launched successfully");
                break;
        }
    }

    protected static enum devices {iOS, Android}
}
