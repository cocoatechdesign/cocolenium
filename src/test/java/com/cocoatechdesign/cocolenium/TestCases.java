package com.cocoatechdesign.cocolenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

/**
 * Created by IntelliJ IDEA
 * User: Saray Gómez
 * Email: cocoatechdesign@gmail.com
 * On 4/12/15
 * To cocolenium project
 */
public class TestCases {
    protected WebDriver driver;
    protected String url;

    protected static enum browsers {firefox, chrome, iexplorer}

    public TestCases(String browser) {

        switch (browsers.valueOf(browser)) {
            case chrome:
                /**
                 * Para configurar el Driver de Chrome tenemos que seguir varios pasos:
                 * Descargar el driver desde: https://sites.google.com/a/chromium.org/chromedriver/downloads
                 * Guardar en una carpeta y obtener la direccion hacia nuestro driver (en mi caso lo tengo incluido como un archivo dentro del proyecto)
                 * Y pasarle como propiedad esa dirección al driver
                 */

                System.setProperty("webdriver.chrome.driver", "C:/cocolenium/Drivers/chromedriver.exe");
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                driver = new ChromeDriver(capabilities);
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                break;

            case firefox:
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                break;

            case iexplorer:
                System.setProperty("webdriver.ie.driver", "C:/cocolenium/Drivers/IEDriverServer.exe");
                capabilities = DesiredCapabilities.internetExplorer();
                driver = new InternetExplorerDriver(capabilities);
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                break;

        }
    }
}


