/*
 * <copyright file="MobileDevices.java" company="UserZoom Technologies SL">
 * Copyright (c) UserZoom ${YEAR}. All Rights Reserved. http://www.userzoom.com/
 *
 * Proprietary and confidential
 *
 * NOTICE: All information contained herein is, and remains the property
 * of UserZoom Technologies SL. The intellectual and technical concepts
 * contained herein are proprietary to UserZoom Technologies SL and
 * may be covered by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law. Dissemination of this
 * information or reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from UserZoom Technologies SL.
 *
 * </copyright>
 * <author>saraygomez</author>
 * <email>saraygomez@userzoom.com</email>
 * <date>2016-6-22</date>
 * <summary>Contains short description of this file</summary>
 */

package com.cocoatechdesign.cocolenium.resources;

/**
 * Created by IntelliJ IDEA
 * User: Saray Gómez
 * Email: sgomez@userzoom.com
 * On 17/6/16
 * To selenium project
 */

public class MobileDevices {
    public static final MobileDeviceInfo IPOD_UDID = new MobileDeviceInfo("114bee3f9f3e8aa5c8882841781a996d6a89ea00", "montse name");
    public static final MobileDeviceInfo IPHONE6_UDID = new MobileDeviceInfo("07d8afbdff0f59602c0e0da2099aa15dba89d083", "Iphone 6");
    public static final MobileDeviceInfo IPHONE6S_UDID = new MobileDeviceInfo("302ac817827250fceaabb7bb6b4bdc4625771be8", "Jordi's iPad");
    public static final MobileDeviceInfo IPAD_UDID = new MobileDeviceInfo("c1f4636a9a9f159fd5688866e345b0ef8c0acb94", "Ipad");
    public static final MobileDeviceInfo IPADMINI_UDID = new MobileDeviceInfo("cd65aed47463f70177e58fce3bdead64396d65d9", "Ipad");
    public static final MobileDeviceInfo SAMSUNG_UDID = new MobileDeviceInfo("", "Samsung 6");
    public static final MobileDeviceInfo IPHONE6_SIMULATOR = new MobileDeviceInfo("", "iPhone 6");

    public static class MobileDeviceInfo {
        public final String udid;
        public String name;

        public MobileDeviceInfo(String udid, String name) {
            this.udid = udid;
            this.name = name;
        }
    }
}

