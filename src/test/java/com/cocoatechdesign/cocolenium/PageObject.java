package com.cocoatechdesign.cocolenium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by IntelliJ IDEA
 * User: Saray Gómez
 * Email: cocoatechdesign@gmail.com
 * On 10/12/15
 * To cocolenium project
 */
public class PageObject {
    protected WebDriver driver;
    protected JavascriptExecutor daleCalo = (JavascriptExecutor) driver;

    public PageObject(WebDriver driver) {
        this.driver = driver;
    }

    public PageObject() {
    }

    public void navegarA(String url) {
        driver.get(url);
    }

    protected PageObject accionesGenerales(AccionDe accionDe) {
        switch (accionDe){
            case GUARDAR:
                break;
            case CANCELAR:
                break;
            case SALIR:
                break;
            default:
                System.out.println("ACCIÓN NO ENCONTRADA, POR FAVOR REVISE EL TEST");
                break;
        }
        return this;
    }

    protected void scrollHasta(WebElement e){
        // Para poder hacer un scroll debemos saber la posición del WebElement.
        Point localizacion = e.getLocation();
        // Una vez que sabemos la posición creamos una función Javascript para ir hacia dicha posición
        String cadena = String.format("javascript:window.scrollTo(%d,%d)", localizacion.getX(), localizacion.getY());
        // Creamos un executor para Javascript al cual le pasamos el driver
        JavascriptExecutor daleCalo = (JavascriptExecutor) driver;
        // Y lanzamos el Script anterio con la posición del elemento dentro del DOM
        daleCalo.executeAsyncScript(cadena);
    }

    public enum AccionDe {GUARDAR, CANCELAR, SALIR}

}
