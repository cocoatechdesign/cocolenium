package com.cocoatechdesign.cocolenium.GTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

/**
 * Created by IntelliJ IDEA
 * User: Saray Gómez
 * Email: cocoatechdesign@gmail.com
 * On 3/12/15
 * To cocolenium project
 */
public class CheckBox {
    WebDriver driver = new FirefoxDriver();
    WebElement mantequilla;

    @Before
    public void startUp() {
        driver.navigate().to("http://ejemplos.cocoatechdesign.com/checkbox.html");
    }

    @Test
    public void checkboxes() {
        // Casilla con ID... EASY!
        driver.findElement(By.id("option1")).click();
        driver.findElement(By.id("option2")).click();
        driver.findElement(By.id("option3")).click();
        driver.findElement(By.id("option4")).click();
        driver.findElement(By.id("option5")).click();

        // Casilla con Name... EASY!
        driver.findElement(By.name("option1")).click();
        driver.findElement(By.name("option2")).click();
        driver.findElement(By.name("option3")).click();
        driver.findElement(By.name("option4")).click();
        driver.findElement(By.name("option5")).click();

        // Casillas con Value
        clickOnCheckBox("Pan");

        // Casilla con XPATH y Option... EASY!
        driver.findElement(By.xpath("//*[@id='option1']")).click();
        driver.findElement(By.xpath("//*[@id='option3']")).click();
        driver.findElement(By.xpath("//*[@id='option5']")).click();

        // Casilla con XPATH y Value... EASY!
        driver.findElement(By.xpath("//*[@value='Pan']")).click();
        driver.findElement(By.xpath("//*[@value='Leche']")).click();
        driver.findElement(By.xpath("//*[@value='Mantequilla']")).click();

        // Comprobar y cambiar un estado a su inversa
        mantequilla =  driver.findElement(By.id("option2"));
        changeCheckboxStatus(mantequilla, false);
        changeCheckboxStatus(mantequilla, true);

    }

    // Buscar Elemento por Value
    public void clickOnCheckBox(String checkboxValue) {
        // Creamos una lista con los elementos de typo Checkbox
        List<WebElement> checkboxList = driver.findElements(By.xpath("//input[@type='checkbox']"));

        // Creamos un For para comprobar cada elemento de la lista checkboxList e imprimir su Value
        for (WebElement checkbox : checkboxList) {
            System.out.println(checkbox.getAttribute("value"));

            // Una vez tenemos la lista generada y sabemos el valor de cada elemento de la misma, clicamos el checkbox que queremos segun el value que estamos buscando
            if (checkbox.getAttribute("value").equals(checkboxValue)) {
                checkbox.click();
            }
        }
    }

    /**
     * Comprobar si un Checkbox esta seleccionado por defecto y activarlo/desctivarlo
     */

    // Cambiamos su status a su inversa o lo deja por defecto.
    public void changeCheckboxStatus(WebElement e, boolean check) {

        // Comprueba el estado del elemento antes de interactuar con el.
        boolean isChecked = e.isSelected();

        // Si no esta seleccionado le cambiamos el estado
        if (isChecked != check) {
            e.click();
        }

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
