package com.cocoatechdesign.cocolenium.GTest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by IntelliJ IDEA
 * User: Saray Gómez
 * Email: cocoatechdesign@gmail.com
 * On 3/12/15
 * To cocolenium project
 */
public class DropDown {
    WebDriver driver = new FirefoxDriver();

    @Before
    public void startUp() {
        driver.navigate().to("http://ejemplos.cocoatechdesign.com/websites/dropdown.html");
    }

    @Test
    public void dropdown() {
        // Buscamos el Desplegable para interactuar con el
        Select dropdownList = new Select(driver.findElement(By.id("cars")));

        // Seleccionamos un elemento según su posición en el Indice del Desplegable
        dropdownList.selectByIndex(1);
        // Comprobamos que se corresponde los valores
        Assert.assertTrue(dropdownList.getFirstSelectedOption().getText().equals("Saab"));

        // Seleccionamos un elemento según el valor que queremos
        dropdownList.selectByValue("audi");
        // Comprobamos que se corresponde los valores
        Assert.assertTrue(dropdownList.getFirstSelectedOption().getText().equals("Audi"));

        // Seleccionamos un elemento según el valor que es visible
        dropdownList.selectByVisibleText("Fiat");
        // Comprobamos que se corresponde los valores
        Assert.assertTrue(dropdownList.getFirstSelectedOption().getText().equals("Fiat"));

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
