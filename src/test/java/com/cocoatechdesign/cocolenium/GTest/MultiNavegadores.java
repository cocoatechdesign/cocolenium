package com.cocoatechdesign.cocolenium.GTest;

import com.cocoatechdesign.cocolenium.TestCases;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

/**
 * Created by IntelliJ IDEA
 * User: Saray Gómez
 * Email: cocoatechdesign@gmail.com
 * On 19/12/2015
 * To cocolenium project
 */
public class MultiNavegadores extends TestCases {

    public MultiNavegadores() {
        super(System.getProperty("browser"));
    }

    @Before
    public void startUp() {
        driver.navigate().to("http://ejemplos.cocoatechdesign.com");
    }

    @Test
    public void checkboxes() {
        // Accedemos a la web
        driver.findElement(By.linkText("CheckBox")).click();

        // Seleccionamos varias opciones para comprobar los checkbox o casillas de verificación
        driver.findElement(By.id("option1")).click();
        driver.findElement(By.id("option2")).click();
        driver.findElement(By.id("option3")).click();
        driver.findElement(By.id("option4")).click();
        driver.findElement(By.id("option5")).click();
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
