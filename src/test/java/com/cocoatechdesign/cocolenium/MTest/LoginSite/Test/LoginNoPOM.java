package com.cocoatechdesign.cocolenium.MTest.LoginSite.Test;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by IntelliJ IDEA
 * User: Saray Gómez
 * Email: cocoatechdesign@gmail.com
 * On 3/12/15
 * To cocolenium project
 */
public class LoginNoPOM {
    WebDriver driver = new FirefoxDriver();

    @Before
    public void startUp() {
        driver.navigate().to("http://ejemplos.cocoatechdesign.com/login.html");
    }

    @Test
    public void loginNoPOM() {
        // Buscamos el elemento Usuario para introducir el nombre de usuario
        driver.findElement(By.xpath("//*[@type='email']")).sendKeys("Nombre del Usuario");
        // Buscamos el elemento Contraseña para introducir la contraseña de usuario
        driver.findElement(By.xpath("//*[@type='password']")).sendKeys("Contraseña del Usuario");
        // Buscamos el boton Enviar y clicamos
        driver.findElement(By.xpath("//*[@type='submit']")).click();

        // Buscamos la cabecera de la pagina y comprobamos que corresponde a la que esperamos
        String cabecera = driver.findElement(By.xpath("html/body/div[2]/div/div/h2")).getText();
        Assert.assertTrue(cabecera.equals("FORMULARIO DE LOGIN"));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
