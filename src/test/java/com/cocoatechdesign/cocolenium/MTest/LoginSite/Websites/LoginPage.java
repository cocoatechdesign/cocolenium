package com.cocoatechdesign.cocolenium.MTest.LoginSite.Websites;

import com.cocoatechdesign.cocolenium.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by IntelliJ IDEA
 * User: Saray Gómez
 * Email: cocoatechdesign@gmail.com
 * On 10/12/15
 * To cocolenium project
 */
public class LoginPage {
    private static String URL_PAGINA = "http://ejemplos.cocoatechdesign.com/login.html";
    private WebDriver driver;

    @CacheLookup
    @FindBy(xpath = "//*[@type='email']")
    private WebElement usuario;
    @FindBy(xpath = "//*[@type='password']")
    private WebElement contraseña;
    @FindBy(linkText = "Login")
    private WebElement login;
    @FindBy(xpath = "html/body/div[2]/div/div/h2")
    private WebElement cabecera;
    @FindBy(xpath = "//*[@type='submit']")
    private WebElement loginBoton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        driver.get(URL_PAGINA);
        PageFactory.initElements(driver, this);
    }

    public LoginPage añadirUsuario(String nombreDeUsuario) {
        this.usuario.clear();
        this.usuario.sendKeys(nombreDeUsuario);
        return this;
    }

    public LoginPage añadirContraseña(String contraseña) {
        this.contraseña.clear();
        this.contraseña.sendKeys(contraseña);
        return this;
    }

    public LoginPage loginCompleto(String nombreDeUsuario, String contraseña) {
        añadirUsuario(nombreDeUsuario);
        añadirContraseña(contraseña);
        loginBoton.click();
        return this;
    }

    // Comprobamos que estamos en la pàgina correcta
    public boolean paginaDeLogin() {
        return cabecera.getText().toString().contains("FORMULARIO DE LOGIN");
    }
}
