package com.cocoatechdesign.cocolenium.MTest.LoginSite.Test;

import com.cocoatechdesign.cocolenium.MTest.LoginSite.Websites.LoginPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by IntelliJ IDEA
 * User: Saray Gómez
 * Email: cocoatechdesign@gmail.com
 * On 10/12/15
 * To cocolenium project
 */
public class Login {
    WebDriver driver;

    @Before
    public void setup() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void loginTest() {
        // Creamos un objeto de tipo LoginPage
        LoginPage loginPage = new LoginPage(driver);

        // Comprobamos que la pagina este abierta y sea la correcta por el Titulo
        Assert.assertTrue(loginPage.paginaDeLogin());

        // Empezamos a pasarle los parametros del Login
        loginPage.añadirUsuario("Cocoa");
        loginPage.añadirContraseña("Tech");

        // Hacemos el Login Completo
        loginPage.loginCompleto("Cocoa", "Tech");
    }

    @After
    public void tearDown() {
        driver.close();
    }
}
